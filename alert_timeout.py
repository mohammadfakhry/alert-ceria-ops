import requests
import pymongo
import os
import sys
from pathlib import Path
from dotenv import load_dotenv

#Kirim pesan Slack via Slack API
#------------------------------------------------------------------------------
#Menghubungkan file .env
env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

#Koneksi ke database
CERIA_DB = os.environ.get('CERIA_DB_KEY')
client = pymongo.MongoClient(CERIA_DB)
proxy = { "http":os.getenv('PROXY_ADD'), "https":os.getenv('PROXY_ADD') }

#Find data dari nama database dan table
db = client["ceriadb"]
mycollection = db["charges"]
find_data = mycollection.find({'status':{'$in':['TIMEOUT',"IN_PROGRESS"]}},{'_id':0,'transaction_details.order_id':1})

#koneksi dan forward message ke SLACK bot
def send_slack_message(message):
    TokenKey = os.getenv('SLACK_API_KEY')
    payload = '{"text":"%s"}' % message
    response = requests.post(TokenKey,
    data = payload,proxies=proxy)
    
    print(response.text)

#membaca dan menyimpan value dari database 
def main(argv):
    message = []
    if find_data != None:
        for x in find_data:
            print(x)
            message.append(x)
    
    if x != None:   
        send_slack_message(message)
           
if __name__ == "__main__":
    main(sys.argv[1:])                  