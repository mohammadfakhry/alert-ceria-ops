from logging import StringTemplateStyle
import psycopg2
import os
import requests
from pathlib import Path
from dotenv import load_dotenv, find_dotenv

#Menghubungkan file .env
load_dotenv(find_dotenv())
env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)
proxy = { "http":os.getenv('PROXY_ADD'), "https":os.getenv('PROXY_ADD') }
conn = psycopg2.connect(dbname=os.getenv('DB_NAME'), user=os.getenv('DB_USER'),password=os.getenv('DB_PASS'),host=os.getenv('DB_HOST'),port=os.getenv('DB_PORT')) 

#koneksi dan forward message ke SLACK bot
def send_slack_message(message):
    TokenKey = os.getenv('SLACK_API_KEY2')
    payload = '{"text":"%s"}' % message
    response = requests.post(TokenKey,
    data = payload,proxies=proxy)
    
    print(response.text)

#
with conn:
    with conn.cursor() as cur:
        cur.execute("SELECT distinct loc_Acct_id FROM custom.CMPDT WHERE LOAN_CREATED = 'N' AND NVL(PURCHASE_AMT - REVERSAL_AMT,'0')>0 AND NVL(CASH_OUT_STATUS,'N') IN ('S','N')  and loan_created = 'N' AND BANK_ID =  '01' having count(merchant_ref_id) > 1 group by  merchant_ref_id ,MERCHANT_ID,loc_Acct_id ;")
        for record in cur.fetchall():
            print(record)
    pesan = "terdapat merchant ref id double pada oda"
    send = pesan + " "+ str(record)

    send_slack_message(send)


conn.close()        